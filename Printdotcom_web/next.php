 <!DOCTYPE html>
  <html>
    <head>
      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>

    <body>
       <nav>
    <div class="nav-wrapper teal lighten-2">
      <a href="#" class="brand-logo">Printdotcom</a>
      <ul id="nav-mobile" class="right hide-on-med-and-down">
        <li><a href="webfix.php">Logout</a></li>    
      </ul>
    </div>
  </nav>
  <!--data pelanggan-->
  <div class="container-fluid blue lighten-4">
  <h1 class="center ">DATA PELANGGAN</h1>
   <table>
        <thead>
          <tr>
              <th>Nama depan</th>
              <th>username</th>
              <th>Email</th>
          </tr>
        </thead>

        <tbody>
          <tr>
            <td>Alvin</td>
            <td>Eclair</td>
            <td>$0.87</td>
          </tr>
          <tr>
            <td>Alan</td>
            <td>Jellybean</td>
            <td>$3.76</td>
          </tr>
          <tr>
            <td>Jonathan</td>
            <td>Lollipop</td>
            <td>$7.00</td>
          </tr>
        </tbody>
      </table>
      </div>
      <!--data pelanggan-->
  <h1 class="center ">DATA PEMESANAN</h1>
   <table>
        <thead>
          <tr>
              <th>Nama</th>
              <th>tanggal pemesanan</th>
              <th>Link File</th>
              <th>Terima</th>
              <th>Tolak</th>
              <th>Informasi Biaya</th>
          </tr>
        </thead>

        <tbody>
          <tr>
            <td>Alvin</td>
            <td>Eclair</td>
            <td>$0.87</td>
            <td><a onclick="M.toast({html: 'terima'})" class="btn">Terima</a> </td>
            <td><a onclick="M.toast({html: 'tolak'})" class="btn">Tolak</a> </td>
            <td>100.000</td>
          </tr>
          <tr>
            <td>Alan</td>
            <td>Jellybean</td>
            <td>$3.76</td>
            <td><a onclick="M.toast({html: 'terima'})" class="btn">Terima</a> </td>
            <td><a onclick="M.toast({html: 'I am a toast'})" class="btn">Tolak</a> </td>
          </tr>
          <tr>
            <td>Jonathan</td>
            <td>Lollipop</td>
            <td>$7.00</td>
            <td><a onclick="M.toast({html: 'I am a toast'})" class="btn">Terima</a> </td>
            <td><a onclick="M.toast({html: 'I am a toast'})" class="btn">Tolak</a> </td>
          </tr>
        </tbody>
      </table>

      <!--JavaScript at end of body for optimized loading-->
      <script type="text/javascript" src="js/materialize.min.js"></script>
    </body>
  </html>