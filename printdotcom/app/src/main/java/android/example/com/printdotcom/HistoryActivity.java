package android.example.com.printdotcom;

import android.example.com.printdotcom.model.Post;
import android.example.com.printdotcom.model.Profile;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class HistoryActivity extends AppCompatActivity {

    ListView listView;
    Query ref,ref2;
    FirebaseDatabase database;
    ArrayList<String> list = new ArrayList<>();
    ArrayList<String> list2 = new ArrayList<>();
    ArrayAdapter<String> adapter, adapter2;
    Post post;
    Profile profile;
    FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        mAuth = FirebaseAuth.getInstance();

        post = new Post();
        profile = new Profile();
        listView = findViewById(R.id.listView);
        database = FirebaseDatabase.getInstance();
        ref = database.getReference("Post").orderByChild("userId").equalTo(mAuth.getUid());
        adapter = new ArrayAdapter<String>(HistoryActivity.this, R.layout.history_info,R.id.tv1,list);

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot ds :dataSnapshot.getChildren())
                {
                    post = ds.getValue(Post.class);
                    profile = ds.getValue(Profile.class);

                    list.add(post.getTimestamp() + "              " + post.getCopi() + "                 " + post.getStatus());
                }
                listView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

//        ref2.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//
//                for (DataSnapshot ds :dataSnapshot.getChildren())
//                {
//                    profile = ds.getValue(Profile.class);
//
//                    list2.add(profile.getName() + "      " + profile.getAddress() + "      " + profile.getPhone());
//                }
//                listView.setAdapter(adapter2);
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });
    }
}
