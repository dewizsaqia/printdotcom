package android.example.com.printdotcom;

import android.content.Intent;
import android.example.com.printdotcom.model.Profile;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;


public class MenuActivity extends AppCompatActivity {

    public static final String table1 = "Post";
    public static final String table2 = "Comment";
    public static final String table3 = "User";

    ImageButton imgProfile;
    TextView nama;
    Button ordr,contact,history,harga;
    Query ref;
    Profile profile;
    FirebaseAuth mAuth;
    FirebaseDatabase database;
    ArrayList<String> list = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();
        profile = new Profile();
        ref = database.getReference("Profile").orderByChild("userId").equalTo(mAuth.getUid());

        imgProfile = findViewById(R.id.gambarProfile);
        ordr = findViewById(R.id.btn_order);
        contact = findViewById(R.id.contactInfo);
        history = findViewById(R.id.history);
        harga = findViewById(R.id.harga);
        nama = findViewById(R.id.namaProfile);

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot ds :dataSnapshot.getChildren())
                {
                    profile = ds.getValue(Profile.class);

                    String Nama = profile.getName();

                    nama.setText(Nama);

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MenuActivity.this,ContactInfo.class);
                startActivity(intent);
            }
        });

        ordr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MenuActivity.this,UploadFileActivity.class);
                startActivity(intent);
            }
        });

        imgProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MenuActivity.this,ViewProfileActivity.class);
                startActivity(intent);
            }
        });

        history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MenuActivity.this,HistoryActivity.class);
                startActivity(intent);
            }
        });

        harga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MenuActivity.this,DaftarHarga.class);
                startActivity(intent);
            }
        });
    }


}
