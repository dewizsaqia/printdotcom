package android.example.com.printdotcom;

import android.*;
import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import android.example.com.printdotcom.model.Post;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.ServerTimestamp;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


public class UploadFileActivity extends AppCompatActivity  implements AdapterView.OnItemSelectedListener{

    EditText copy, other;
    TextView notif;
    Spinner sp1,sp2,sp3;
    Button upload, order;

    DatabaseReference ref;
    Post post;
    Uri pdfUri;
    FirebaseStorage storage;
    FirebaseDatabase database;
    FirebaseAuth mAuth;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_file);

    storage=FirebaseStorage.getInstance();
    database=FirebaseDatabase.getInstance();
    mAuth=FirebaseAuth.getInstance();
    ref = database.getReference("Post");
    post = new Post();

    notif = findViewById(R.id.namaFile);
    upload = findViewById(R.id.btn_file);
    order = findViewById(R.id.btnOrder);
    copy =  findViewById(R.id.coppies);
    other = findViewById(R.id.other);

    sp1 = (Spinner) findViewById(R.id.spinnerCol);
    sp2 = findViewById(R.id.spinnerPap);
    sp3 = findViewById(R.id.spinnerOr);

        //spinner untuk colour
        ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.createFromResource(this, R.array.colour, android.R.layout.simple_spinner_item);
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp1.setAdapter(adapter1);
        sp1.setOnItemSelectedListener(this);

        //spinner untuk paper size
        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this, R.array.size, android.R.layout.simple_spinner_item);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp2.setAdapter(adapter2);
        sp2.setOnItemSelectedListener(this);

        //spinner untuk orientation
        ArrayAdapter<CharSequence> adapter3 = ArrayAdapter.createFromResource(this, R.array.orientation, android.R.layout.simple_spinner_item);
        adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp3.setAdapter(adapter3);

        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(ContextCompat.checkSelfPermission(UploadFileActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)== PackageManager.PERMISSION_GRANTED){
                    selectPdf();
                }else
                    ActivityCompat.requestPermissions(UploadFileActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 9);

            }
        });

        order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(pdfUri!=null)
                    uploadFile(pdfUri);
                else
                    Toast.makeText(UploadFileActivity.this,"Select a File",Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getValues()
    {
        Calendar calendarExpired = Calendar.getInstance();
        String dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", new Locale("en")).format(calendarExpired.getTime());

        post.setColour(sp1.getSelectedItem().toString());
        post.setSize(sp2.getSelectedItem().toString());
        post.setOrientation(sp3.getSelectedItem().toString());
        post.setCopi(copy.getText().toString());
        post.setOthr(other.getText().toString());
        post.setTimestamp(dateFormat);
        post.setUserId(mAuth.getUid());
    }

    private void uploadFile(Uri pdfUri) {

        final String id = ref.push().getKey();

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                getValues();
                ref.child(id).setValue(post);
                Toast.makeText(UploadFileActivity.this,"Data Inserted...",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        progressDialog = new ProgressDialog(this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setTitle("Uploading File...");
        progressDialog.setProgress(0);
        progressDialog.show();

        final String fileName = System.currentTimeMillis()+"";
        StorageReference storageReference = storage.getReference();

        storageReference.child("Uploads").child(fileName).putFile(pdfUri)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                        String url = taskSnapshot.getDownloadUrl().toString();

                        DatabaseReference reference = database.getReference();
                        reference.child(fileName).setValue(url).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if(task.isSuccessful()) {
                                    Toast.makeText(UploadFileActivity.this, "File Succesfully Uploaded", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(UploadFileActivity.this, MenuActivity.class);
                                    startActivity(intent);
                                }else
                                    Toast.makeText(UploadFileActivity.this,"File Not Succesfully Uploaded",Toast.LENGTH_SHORT).show();

                            }
                        });

                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

                Toast.makeText(UploadFileActivity.this,"File Not Succesfully Uploaded",Toast.LENGTH_SHORT).show();


            }
        }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {

                int currentProgress =(int) (100*taskSnapshot.getBytesTransferred()/taskSnapshot.getTotalByteCount());
                progressDialog.setProgress(currentProgress);
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode==9 && grantResults[0]==PackageManager.PERMISSION_GRANTED){
            selectPdf();
        }else
            Toast.makeText(UploadFileActivity.this,"please provide permission",Toast.LENGTH_SHORT).show();

    }

    private void selectPdf() {
        Intent intent = new Intent();
        intent.setType("application/pdf");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, 86);
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode==86 && resultCode==RESULT_OK && data!=null){
            pdfUri=data.getData();
            notif.setText("A file is selected : " + data.getData().getLastPathSegment());
        }else{
            Toast.makeText(UploadFileActivity.this,"Please select files",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
