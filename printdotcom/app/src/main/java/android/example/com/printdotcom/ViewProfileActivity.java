package android.example.com.printdotcom;

import android.*;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.example.com.printdotcom.model.Profile;
import android.example.com.printdotcom.model.User;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class ViewProfileActivity extends AppCompatActivity {

    Button logout, save, upload;
    TextView notif;
    EditText name, phone, address;
    private FirebaseAuth mAuth;
    FirebaseDatabase database;
    FirebaseStorage storage;
    Profile profile;
    DatabaseReference ref;
    ProgressDialog progressDialog;
    Uri uri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_profile);

        storage=FirebaseStorage.getInstance();
        database=FirebaseDatabase.getInstance();
        profile = new Profile();
        mAuth = FirebaseAuth.getInstance();
        ref = database.getReference("Profile");

        notif = findViewById(R.id.notif);
        save = findViewById(R.id.save);
        logout = findViewById(R.id.logout);
        name = findViewById(R.id.editText);
        phone = findViewById(R.id.editText2);
        address = findViewById(R.id.editText4);
        upload = findViewById(R.id.button2);

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mAuth.signOut();
                Intent intent = new Intent(ViewProfileActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(ContextCompat.checkSelfPermission(ViewProfileActivity.this, android.Manifest.permission.READ_EXTERNAL_STORAGE)== PackageManager.PERMISSION_GRANTED){
                    selectJpg();
                }else
                    ActivityCompat.requestPermissions(ViewProfileActivity.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, 9);
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (uri != null)
                    uploadFile(uri);
                else
                    Toast.makeText(ViewProfileActivity.this, "Select a File", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getValues() {
        Calendar calendarExpired = Calendar.getInstance();
        String dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", new Locale("en")).format(calendarExpired.getTime());

        profile.setName(name.getText().toString());
        profile.setPhone(phone.getText().toString());
        profile.setAddress(address.getText().toString());
        profile.setUserId(mAuth.getUid());
        profile.setTimestamp(dateFormat);

    }

    private void uploadFile(Uri uri) {

        final String id = ref.push().getKey();

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                getValues();
                ref.child(id).setValue(profile);
                Toast.makeText(ViewProfileActivity.this, "Data Inserted...", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        progressDialog = new ProgressDialog(this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setTitle("Uploading File...");
        progressDialog.setProgress(0);
        progressDialog.show();

        final String fileName = System.currentTimeMillis()+"";
        StorageReference storageReference = storage.getReference();

            storageReference.child("Uploads").child(fileName).putFile(uri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                String url = taskSnapshot.getDownloadUrl().toString();

                DatabaseReference reference = database.getReference();
                reference.child(fileName).setValue(url).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if(task.isSuccessful()) {
                            Toast.makeText(ViewProfileActivity.this, "File Succesfully Uploaded", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(ViewProfileActivity.this, MenuActivity.class);
                            intent.putExtra("name",name.getText());
                            startActivity(intent);
                        }else
                            Toast.makeText(ViewProfileActivity.this,"File Not Succesfully Uploaded",Toast.LENGTH_SHORT).show();

                    }
                });

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

                Toast.makeText(ViewProfileActivity.this,"File Not Succesfully Uploaded",Toast.LENGTH_SHORT).show();


            }
        }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {

                int currentProgress =(int) (100*taskSnapshot.getBytesTransferred()/taskSnapshot.getTotalByteCount());
                progressDialog.setProgress(currentProgress);
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode==9 && grantResults[0]== PackageManager.PERMISSION_GRANTED){
            selectJpg();
        }else
            Toast.makeText(ViewProfileActivity.this,"please provide permission",Toast.LENGTH_SHORT).show();

    }

    private void selectJpg() {
        Intent intent = new Intent();
        intent.setType("application/jpg");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, 86);
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode==86 && resultCode==RESULT_OK && data!=null){
            uri=data.getData();
            notif.setText("A file is selected : " + data.getData().getLastPathSegment());
        }else{
            Toast.makeText(ViewProfileActivity.this,"Please select files",Toast.LENGTH_SHORT).show();
        }
    }

}
