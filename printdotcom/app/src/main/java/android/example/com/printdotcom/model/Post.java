package android.example.com.printdotcom.model;

/**
 * Created by TOSHIBA on 4/16/2018.
 */

public class Post {
    String id;
    String userId;
    private String colour;
    private String post;
    private String copi;
    private String othr;
    private String size;
    private String orientation;
    Object timestamp;
    String status;

    //Wajib kasih Constructor Kosong
    public Post() {

    }

    public Post(String id, String colour, String post, String copi, String othr, String size, String orientation, Object timestamp,String userId,String status) {
        this.id = id;
        this.colour = colour;
        this.post = post;
        this.copi = copi;
        this.othr = othr;
        this.size = size;
        this.orientation = orientation;
        this.timestamp = timestamp;
        this.userId = userId;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public String getCopi() {
        return copi;
    }

    public void setCopi(String copi) {
        this.copi = copi;
    }

    public String getOthr() {
        return othr;
    }

    public void setOthr(String othr) {
        this.othr = othr;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getOrientation() {
        return orientation;
    }

    public void setOrientation(String orientation) {
        this.orientation = orientation;
    }

    public Object getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Object timestamp) { this.timestamp = timestamp;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
    public String getUserId() {
        return userId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}