package android.example.com.printdotcom.model;

/**
 * Created by TOSHIBA on 5/5/2018.
 */

public class Profile {
    String userId;
    String name;
    String phone;
    String address;
    Object timestamp;


    public Profile() {
    }

    public Profile(String name, String phone, String address,String userId,Object timestamp) {
        this.name = name;
        this.phone = phone;
        this.address = address;
        this.userId = userId;
        this.timestamp = timestamp;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Object getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Object timestamp) { this.timestamp = timestamp;
    }
}



