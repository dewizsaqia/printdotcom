package android.example.com.printdotcom.model;

/**
 * Created by TOSHIBA on 4/15/2018.
 */

public class User {

    String userId;
    String username;
    String email;
//    String name;
//    String phone;
//    String address;

    public User() {
    }

    public User(String userId, String username, String email) {
        this.userId = userId;
        this.username = username;
        this.email = email;
    }

    public String getUserId() {
        return userId;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

}