<!DOCTYPE html>
<html>
<head>
    <title> Notifikasi Admin </title>
    <script type="text/javascript">
        function notifyMe(msg_title, msg_body, redirect_onclick) {
            var granted = 0;
            if (!("Notification" in window)) {
                alert("This Browser does not support dekstop notification");
            }
            else if (Notfication.permission === "granted") {
                granted = 1;
            }
            else if (Notfication.permission !== "denied") {
                Notfication.requestPermission(function (permisson) {

                }
            }
        });
    }
    if (granted == 1){
        var notification = new Notfication(msg_title, {
        body: msg_body,
        icon: 'icon-bell.png'
    });
    if (redirect_onclick){
        notication.onclick = function() {
            window.location.href = redirect_onclick;
        }
        }
    }
}
</script>
</head>
<body>
    <hr> Notifikasi<hr/>
    <button onclick="notifyMe('Notfication', 'Ada Notifikasi, silahkan cek', 'Notifikasi baru')">Notify Me!</button>
</body>
</html>


    </script>