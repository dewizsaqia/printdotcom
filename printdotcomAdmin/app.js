(function() {


  const config = {
    apiKey: "AIzaSyBAqM2gRFJDGXELhl_CqsqKujDOLL1HWRc",
    authDomain: "printdotcom-11e63.firebaseapp.com",
    databaseURL: "https://printdotcom-11e63.firebaseio.com",
    projectId: "printdotcom-11e63",
    storageBucket: "printdotcom-11e63.appspot.com",
    messagingSenderId: "580041176214"
  };
  firebase.initializeApp(config);

  //Get elements
  const txtEmail = document.getElementById('txtEmail');
  const txtPassword = document.getElementById('txtPassword');
  const btnLogin = document.getElementById('btnLogin');
  const btnSignUp = document.getElementById('btnSignUp');

  //add login event
  btnLogin.addEventListener('click', e => {
    //Get email and pass
    const email = txtEmail.value;
    const pass = txtPassword.value;
    const auth = firebase.auth();
    //Sign In
    const promise = auth.signInWithEmailAndPassword(email,password);
    promise.catch(e => console.log(e.message));
  
  });

  // add realtime listener
  firebase.auth().onAuthStateChanged(firebaseUser => {
    if(firebaseUser){
      console.log(firebaseUser);
    }else {
      console.log('not logged in');
    }
  });